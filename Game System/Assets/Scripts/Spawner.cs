﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //declare variables
    public GameObject blankDie;
    public static float spawnY;

    // Start is called before the first frame update
    void Start()
    {
        spawnY = transform.position.y; //y spawn position

        List<Vector3> spawnPoints = new List<Vector3>(); //list of spawn points
        for (int i = 0; i < transform.childCount; i++) //looping through all children of game object
        {
            spawnPoints.Add(transform.GetChild(i).position); //spawn a die
        }

        for (int i = 0; i < spawnPoints.Count; i++) //looping through spawn points
        {
            Instantiate(blankDie, spawnPoints[i], Quaternion.identity); //make the die
        }
        GameObject[] dice = GameObject.FindGameObjectsWithTag("Die"); //find "Die" tag on game object
        for (int i = 0; i < dice.Length; i++)
        {
            dice[i].GetComponent<Die>().switchStates = true;
        }
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            Instantiate(blankDie, spawnPoints[i], Quaternion.identity);
        }
    }
}

