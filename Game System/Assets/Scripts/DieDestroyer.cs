﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Die>().loseHealth) //Die collision with Destroyer
        {
            GameManager.health--; //deduct health in GameManager & on screen
        }
        Destroy(collision.gameObject); //destroy die
    }
}
