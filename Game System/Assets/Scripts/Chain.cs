﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chain : MonoBehaviour
{
    //declare variables
    Text text;
    public static int chain = 0;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>(); //get text component
    }

    // Update is called once per frame
    void Update()
    {
        text.text = chain + " chain"; //show and updates chain on screen
    }
}
