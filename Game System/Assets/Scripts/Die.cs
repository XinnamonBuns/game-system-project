﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : MonoBehaviour
{
    //declare variables
    public TextMesh textMesh;
    private SpriteRenderer spriteRendererComponent;

    public int dieNumber;
    public Material[] colours;
    public int materialNumber;
    public float speed;
    public float yTop;
    public float yBottom;
    public Vector3 finalPosition;
    public bool switchStates = false;
    public bool loseHealth = false;

    private int state;
    private Vector3 startPosition;
    public Vector3 destination;
    private float lerpTime = 0;

    // Start is called before the first frame update
    void Awake()
    {
        //load in starting position of dice
        startPosition = transform.position;
        destination = transform.position;
        destination.y = yTop;

        dieNumber = Random.Range(1, 7); //random number
        textMesh.text = dieNumber.ToString();

        spriteRendererComponent = GetComponent<SpriteRenderer>(); //get spriterenderer component
        materialNumber = Random.Range(0, 5); //random color 
        spriteRendererComponent.material = colours[materialNumber]; //get matrial of spriterenderer component

        finalPosition = GameObject.Find("Final Destination").transform.position; //find the final destination
    }

    // Update is called once per frame
    void Update()
    {
        //lerp dice to target position
        lerpTime += speed;
        transform.position = Vector3.Lerp(startPosition, destination, lerpTime);

        if (switchStates) //telling dice to moved to next position
        {
            if (state == 2) //final target position
            {
                state++;
                startPosition = transform.position;
                destination.y -= 5;
                lerpTime = 0;
            }
            if (state == 0) //1st target position
            {
                state++;
                startPosition = transform.position;
                destination.y = yBottom;
                lerpTime = 0;
            }
            switchStates = false;
        }
    }

    void OnMouseDown()
    {
        if (state == 2) // clicked on final target position
        {
            //die at final position moves down and player loses health, breaks chain back to 0
            state++;
            startPosition = transform.position;
            destination.y -= 5;
            lerpTime = 0;
            Chain.chain = 0;
            loseHealth = true;
        }
        if (state == 1) //if clicked on 2nd target position
        {
            bool correctDieNumber = false; //cant click on false correctdienumber

            //if the number that's clicked is 1 greater than the final position or if it's 1 when the final position number is 6
            if (GameManager.finalDieNumber + 1 == dieNumber || (GameManager.finalDieNumber == 6 && dieNumber == 1))
            {
                correctDieNumber = true; //the number is correct
            }

            //if the number is correct, or if the colour is the same, or if the final position is empty
            if (correctDieNumber || GameManager.finalMaterialNumber == materialNumber || FindFinalDie() == null) 
            {
                //call gameobject function to move die in 1st position to 2nd position
                Die dieUp = FindDieUp();
                if (dieUp != null)
                {
                    dieUp.switchStates = true;
                }

                //call gameobject function to move die from 2nd position to final position
                Die dieFinal = FindFinalDie();
                if (dieFinal != null)
                {
                    dieFinal.switchStates = true;
                }

                //die at final target position
                state++;
                startPosition = transform.position; 
                destination = finalPosition;
                lerpTime = 0;
                GameManager.finalDieNumber = dieNumber;
                GameManager.finalMaterialNumber = materialNumber;
                Score.score++; 
                Chain.chain++;
                Instantiate(gameObject, new Vector3(transform.position.x, Spawner.spawnY, 0), Quaternion.identity);
            }
        }
    }
    

    private Die FindDieUp() //gameobject function
    {
        //loop between all dice to find and return the one above this die; move die from 1st position to 2nd position
        Die foundDie = null;
        GameObject[] dice = GameObject.FindGameObjectsWithTag("Die"); //take all dice and put into array
        for (int i = 0; i < dice.Length; i++)
        {
            Vector2 diePos = dice[i].transform.position;
            Vector2 targetLocation = new Vector2(transform.position.x, yTop);
            if (Vector2.Distance(diePos, targetLocation) < 0.1)
            {
                foundDie = dice[i].GetComponent<Die>();
            }
        }
        return foundDie; //move the die
    }

    private Die FindFinalDie()
    {
        //loop between all dice to find and return the one above this die; move die from 2nd position to final position
        Die foundDieScript = null;
        GameObject[] dice = GameObject.FindGameObjectsWithTag("Die");
        for (int i = 0; i < dice.Length; i++)
        {
            Die dieScript = dice[i].GetComponent<Die>();
            if (dieScript.state == 2)
            {
                foundDieScript = dieScript;
            }
        }
        return foundDieScript;
    }
}
