﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // declare variables
    public static int health = 6; //total health
    public static int finalMaterialNumber;
    public static int finalDieNumber;

    public Transform healthBar;
    private List<GameObject> healthBalls = new List<GameObject>(); //list of balls for health
        
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < healthBar.childCount; i++) //loop through child for health count
        {
            healthBalls.Add(healthBar.GetChild(i).gameObject); //add game object
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < healthBalls.Count; i++) //for the looped health balls
        {
            if (health <= i)
            {
                healthBalls[i].SetActive(false); //lose health
            }
        }
        if (health <= 0) //if health is zero
        {
            SceneManager.LoadScene("GameOver"); //load game over screen
        }
    }
}
