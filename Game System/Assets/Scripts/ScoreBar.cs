﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
    // declare variables
    Slider slider;
    public Image sliderFill;
    public AnimationCurve animCurve;
    float percentage;

    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>(); //get slider component
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = Score.score; //slider's position = total score

        if (slider.value == 50) //when score hits 50/50, bar will lerp flash
        {
            sliderFill.color = Color.Lerp(new Color32(253,53,205,255), Color.white, animCurve.Evaluate(percentage));
            if(percentage < 2)
            percentage += 10 * Time.deltaTime;
            else
            {
                percentage = 0; 
            }
        }


    }

}
